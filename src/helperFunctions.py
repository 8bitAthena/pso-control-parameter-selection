from Benchmark import Benchmark
import os

def gen_bms():
    bms = []

    # Set the domain for the x-values of each benchmark function
    for i in range(1,7):
        
        if (i == 1):
            #ripple 25
            bms.append(Benchmark(i, 0, 1))
        elif (i == 2):
            #exponential
            bms.append(Benchmark(i, -1, 1))
        elif (i == 3):
            #discus
            bms.append(Benchmark(i, -100, 100))
        elif (i == 4):
            #rosenbrock
            bms.append(Benchmark(i, -30, 30))
        # elif (i == 5):
        #     #qing
        #     bms.append(Benchmark(i, -500, 500))
        # elif (i == 6):
        #     #brown
        #     bms.append(Benchmark(i, -1, 4))
            
    return bms

def gen_path(mode, bm):
    #Generate the necessary filepath to write data to
    filename = "data" + str(bm) + ".csv"
    path = ""

    if (mode == 1):
        path = "fixedCtrlParams"
    elif (mode == 2):
        path = "itrSampling"
    elif (mode == 3):
        path = "stagnSampling"
    elif mode == 4:
        path = "srItrSampling"
    elif mode == 5:
        path = "srStagnSampling"
    
    return os.path.join(path, filename)

def make_dir(mode):
    #Make directories to store data in
    if (mode == 1):
        os.makedirs("fixedCtrlParams")
    elif (mode == 2):
        os.makedirs("itrSampling")
    elif (mode == 3):
        os.makedirs("stagnSampling")
    elif (mode == 4):
        os.makedirs("srItrSampling")
    elif (mode == 5):
        os.makedirs("srStagnSampling")

def init_csv_header(numPart):
    header = ['itr']
    for i in range (1, numPart+1):
        header.append('p' + str(i))

    return header