from Particle import Particle
import sys
import numpy as np
import csv
from helperFunctions import gen_path, init_csv_header

class SetPSO:
    def __init__(self) -> None:
        pass
    
    def itrPSO(self, mode, dim, numPart, curr_bm):

        f = open(gen_path(mode, curr_bm.id), 'w')
        writer = csv.writer(f)
        writer.writerow(init_csv_header(numPart))
        w = 0.7
        c1 = 1.4
        c2 = 1.4

        # 20 runs for each benchmark
        for l in range(20):
            gbest_obj = sys.float_info.max
            gbest = [0.0]* dim
            itr = 0
            swarm = [Particle(dim, curr_bm.xmin, curr_bm.xmax) for i in range(numPart)]

            for i in range (numPart):
                currp = swarm[i]
                fx = curr_bm.eval(currp)
                currp.set_pbest_obj(fx)
                if (fx < gbest_obj):
                    gbest_obj = fx
                    gbest = currp.x.copy()

            while (itr < 500):
                row = []
                if (itr % 10 == 0):
                    row.append(itr)

                for i in range(numPart):
                    #Sample control parameters
                    w = np.random.uniform()
                    rhs = 24*(1-w**2)/(7 - 5*w)
                    mag = np.sqrt(rhs**2)
                    c1 = 0.5 * mag
                    c2 = 0.5 * mag
                    for j in range(dim):
                        r_1j = np.random.uniform(0, 1)
                        r_2j = np.random.uniform(0, 1)
                        currp = swarm[i]
                        currp.v[j] = (w * currp.v[j]) + (c1 * r_1j * (currp.pbest[j] - currp.x[j])) + (c2 * r_2j * (gbest[j] - currp.x[j]))


                    #use v to calculate new x
                    for m in range(dim):
                        currp.x[m] = currp.x[m] + currp.v[m]

                    fx = curr_bm.eval(currp)

                    if (fx < currp.pbest_obj):
                        currp.set_pbest_obj(fx)
                        currp.pbest = currp.x.copy()

                    if (fx < gbest_obj):
                        gbest_obj = fx
                        gbest = currp.x.copy()

                # Want gbest for each particle at each iteration
                    if (itr % 10 == 0):
                        row.append(str(currp.pbest_obj))
                itr += 1

                writer.writerow(row)

            # Will output the gbest for each i
            print("gbest obj val:")
            print(gbest_obj)
    
    def fixedParamsPSO(self, mode, dim, numPart, curr_bm):
        f = open(gen_path(mode, curr_bm.id), 'w')
        writer = csv.writer(f)
        writer.writerow(init_csv_header(numPart))

        # 20 runs for each benchmark
        for l in range(20):
            gbest_obj = sys.float_info.max
            gbest = [0.0]* dim
            itr = 0
            swarm = [Particle(dim, curr_bm.xmin, curr_bm.xmax) for i in range(numPart)]

            for i in range (numPart):
                currp = swarm[i]
                fx = curr_bm.eval(currp)
                currp.set_pbest_obj(fx)
                if (fx < gbest_obj):
                    gbest_obj = fx
                    gbest = currp.x.copy()

            while (itr < 500):
                row = []
                if (itr % 10 == 0):
                    row.append(itr)

                for i in range(numPart):
                    for j in range(dim):
                        r_1j = np.random.uniform(0, 1)
                        r_2j = np.random.uniform(0, 1)
                        currp = swarm[i]
                        currp.v[j] = (0.7 * currp.v[j]) + (1.4 * r_1j * (currp.pbest[j] - currp.x[j])) + (1.4 * r_2j * (gbest[j] - currp.x[j]))

                    #use v to calculate new x
                    for m in range(dim):
                        currp.x[m] = currp.x[m] + currp.v[m]

                    fx = curr_bm.eval(currp)

                    if (fx < currp.pbest_obj):
                        currp.set_pbest_obj(fx)
                        currp.pbest = currp.x.copy()

                    if (fx < gbest_obj):
                        gbest_obj = fx
                        gbest = currp.x.copy()

                # Want gbest for each particle at each iteration
                    if (itr % 10 == 0):
                        row.append(str(currp.pbest_obj))
                itr += 1

                writer.writerow(row)

            # Will output the gbest for each i
            print("gbest obj val:")
            print(gbest_obj)

    def stagnPSO(self, mode, dim, numPart, curr_bm):
        f = open(gen_path(mode, curr_bm.id), 'w')
        writer = csv.writer(f)
        writer.writerow(init_csv_header(numPart))
        w = np.random.uniform()
        rhs = 24*(1-w**2)/(7 - 5*w)
        mag = np.sqrt(rhs**2)
        c1 = 0.5 * mag
        c2 = 0.5 * mag

        pbestcnt = 0

        # 20 runs for each benchmark
        for l in range(20):
            gbest_obj = sys.float_info.max
            gbest = [0.0]* dim
            itr = 0
            swarm = [Particle(dim, curr_bm.xmin, curr_bm.xmax) for i in range(numPart)]

            for i in range (numPart):
                currp = swarm[i]
                fx = curr_bm.eval(currp)
                currp.set_pbest_obj(fx)
                if (fx < gbest_obj):
                    gbest_obj = fx
                    gbest = currp.x.copy()

            while (itr < 500):
                row = []
                if (itr % 10 == 0):
                    row.append(itr)

                for i in range(numPart):
                    for j in range(dim):
                        r_1j = np.random.uniform(0, 1)
                        r_2j = np.random.uniform(0, 1)
                        currp = swarm[i]
                        currp.v[j] = (w * currp.v[j]) + (c1 * r_1j * (currp.pbest[j] - currp.x[j])) + (c2 * r_2j * (gbest[j] - currp.x[j]))

                    #use v to calculate new x
                    for m in range(dim):
                        currp.x[m] = currp.x[m] + currp.v[m]

                    fx = curr_bm.eval(currp)

                    if (fx < currp.pbest_obj):
                        currp.pbestcnt = 0
                        currp.set_pbest_obj(fx)
                        currp.pbest = currp.x.copy()
                    else:
                        currp.pbestcnt += 1

                    if (fx < gbest_obj):
                        gbest_obj = fx
                        gbest = currp.x.copy()

                    if ((currp.pbestcnt % 3 == 0) and (currp.pbestcnt != 0)):
                        w = np.random.uniform()
                        rhs = 24*(1-w**2)/(7 - 5*w)
                        mag = np.sqrt(rhs**2)
                        c1 = 0.5 * mag
                        c2 = 0.5 * mag

                # Want gbest for each particle at each iteration
                    if (itr % 10 == 0):
                        row.append(str(currp.pbest_obj))
                itr += 1

                writer.writerow(row)

            # Will output the gbest for each i
            print("gbest obj val:")
            print(gbest_obj)

    def srItrPSO(self,mode, dim, numPart, curr_bm):
        f = open(gen_path(mode, curr_bm.id), 'w')
        writer = csv.writer(f)
        writer.writerow(init_csv_header(numPart))
        w = 0.7
        c1 = 1.4
        c2 = 1.4

        # 20 runs for each benchmark
        for l in range(20):
            gbest_obj = sys.float_info.max
            gbest = [0.0]* dim
            itr = 0
            swarm = [Particle(dim, curr_bm.xmin, curr_bm.xmax) for i in range(numPart)]

            for i in range (numPart):
                currp = swarm[i]
                fx = curr_bm.eval(currp)
                currp.set_pbest_obj(fx)
                if (fx < gbest_obj):
                    gbest_obj = fx
                    gbest = currp.x.copy()

            while (itr < 500):
                row = []
                if (itr % 10 == 0):
                    row.append(itr)

                for i in range(numPart):
                    w = np.random.uniform()
                    rhs = (24 - (24*(w**2)))/(7-(5*w))
                    lhs = (22-(30*w**2))/(7-(5*w))
                    mid = np.random.uniform(lhs,rhs)
                    c1 = 0.4 * mid
                    c2 = 0.6 * mid

                    for j in range(dim):
                        r_1j = np.random.uniform(0, 1)
                        r_2j = np.random.uniform(0, 1)
                        currp = swarm[i]
                        currp.v[j] = (w * currp.v[j]) + (c1 * r_1j * (currp.pbest[j] - currp.x[j])) + (c2 * r_2j * (gbest[j] - currp.x[j]))

                    #use v to calculate new x
                    for m in range(dim):
                        currp.x[m] = currp.x[m] + currp.v[m]

                    fx = curr_bm.eval(currp)

                    if (fx < currp.pbest_obj):
                        currp.set_pbest_obj(fx)
                        currp.pbest = currp.x.copy()

                    if (fx < gbest_obj):
                        gbest_obj = fx
                        gbest = currp.x.copy()

                # Want gbest for each particle at each iteration
                    if (itr % 10 == 0):
                        row.append(str(currp.pbest_obj))
                itr += 1

                writer.writerow(row)

            # Will output the gbest for each i
            print("gbest obj val:")
            print(gbest_obj)
    
    def srStagnPSO(self, mode, dim, numPart, curr_bm):
        f = open(gen_path(mode, curr_bm.id), 'w')
        writer = csv.writer(f)
        writer.writerow(init_csv_header(numPart))
        w = 0.7
        c1 = 1.4
        c2 = 1.4

        # 20 runs for each benchmark
        for l in range(20):
            gbest_obj = sys.float_info.max
            gbest = [0.0]* dim
            itr = 0
            swarm = [Particle(dim, curr_bm.xmin, curr_bm.xmax) for i in range(numPart)]

            for i in range (numPart):
                currp = swarm[i]
                fx = curr_bm.eval(currp)
                currp.set_pbest_obj(fx)
                if (fx < gbest_obj):
                    gbest_obj = fx
                    gbest = currp.x.copy()

            while (itr < 500):
                row = []
                if (itr % 10 == 0):
                    row.append(itr)

                for i in range(numPart):
                    w = np.random.uniform()
                    rhs = (24 - (24*(w**2)))/(7-(5*w))
                    lhs = (22-(30*w**2))/(7-(5*w))
                    mid = np.random.uniform(lhs,rhs)
                    c1 = 0.4 * mid
                    c2 = 0.6 * mid

                    for j in range(dim):
                        r_1j = np.random.uniform(0, 1)
                        r_2j = np.random.uniform(0, 1)
                        currp = swarm[i]
                        currp.v[j] = (w * currp.v[j]) + (c1 * r_1j * (currp.pbest[j] - currp.x[j])) + (c2 * r_2j * (gbest[j] - currp.x[j]))

                    #use v to calculate new x
                    for m in range(dim):
                        currp.x[m] = currp.x[m] + currp.v[m]

                    fx = curr_bm.eval(currp)

                    if (fx < currp.pbest_obj):
                        currp.set_pbest_obj(fx)
                        currp.pbest = currp.x.copy()

                    if (fx < gbest_obj):
                        gbest_obj = fx
                        gbest = currp.x.copy()

                # Want gbest for each particle at each iteration
                    if (itr % 10 == 0):
                        row.append(str(currp.pbest_obj))
                itr += 1

                writer.writerow(row)

            # Will output the gbest for each i
            print("gbest obj val:")
            print(gbest_obj)