from helperFunctions import gen_bms, make_dir
from setPSO import SetPSO

def execute_pso(mode, dim, numPart, curr_bm):
    select = SetPSO
    if (mode == 1):
        select.fixedParamsPSO(select, mode, dim, numPart, curr_bm)
    elif (mode == 2):
        select.itrPSO(select, mode, dim, numPart, curr_bm)
    elif (mode == 3):
        select.stagnPSO(select, mode, dim, numPart, curr_bm)
    elif (mode == 4):
        select.srItrPSO(select,mode, dim, numPart, curr_bm)
    elif (mode == 5):
        select.srStagnPSO(select, mode, dim, numPart, curr_bm)

def main():
    dim = 10
    numPart = 30
    benchmarks = gen_bms()

    for mode in range(1,6):
        make_dir(mode)
        for j in range(len(benchmarks)):
            curr_bm = benchmarks[j]
            print("current benchmark:" + str(j + 1))
            
            # Executes for 20 independent runs
            execute_pso(mode, dim, numPart, curr_bm)


if __name__ == "__main__":
    main()
