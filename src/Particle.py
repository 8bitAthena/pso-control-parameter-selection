import numpy as np

class Particle:
    def __init__(self, di, minx, maxx):
        self.dim = di
        self.xmax = maxx
        self.xmin = minx
        #init the arrays

        self.v = [0.0]* di
        self.pbest = [0.0]* di
        self.pbestcnt = 0

        #fill particles pos with random float between xmin and xmax
        self.x = [0.0] * di
        for i in range(di):
            #returns random float between range
            self.x[i] = np.random.uniform(minx,maxx)

        self.pbest = self.x.copy()

    def set_pbest_obj(self, obj):
        self.pbest_obj = obj
