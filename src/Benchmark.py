import numpy as np

class Benchmark:

    def __init__ (self, bm, min_x, max_x):
        self.id = bm
        self.xmin = min_x
        self.xmax = max_x

    def eval (self, p):
        sum = 0

        if (self.id == 1):
            for i in range(p.dim):
                sum = sum + (-np.exp(-2* np.log(2) * ((p.x[i] - 0.1)/0.8)**2) * np.sin(5*np.pi*p.x[i])**6)
            return sum

        if (self.id == 2):
            for i in range(p.dim):
                sum = sum + p.x[i]**2
            return -np.exp(-0.5 * sum)

        if (self.id == 3):
            for i in range(2,p.dim):
                sum = sum + p.x[i]**2
            sum = sum + ((10**6) * (p.x[1]**2))
            return sum

        if (self.id == 4):
            for i in range(p.dim - 1):
                sum = sum + 100*(p.x[i+1] - p.x[i]**2)**2 + (p.x[i] - 1)**2
            return sum

        if (self.id == 5):
            for i in range(p.dim):
                sum = sum + (p.x[i]**2 - i)**2
            return sum

        if (self.id == 6):
            for i in range(p.dim - 1):
                sum = sum + ((p.x[i]**2)**((p.x[i+1]**2) + 1)) + ((p.x[i+1]**2)**((p.x[i]**2) + 1))
            return sum
