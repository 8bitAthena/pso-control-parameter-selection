## Read me

### Project Desription

A full description of the theory behind this project can be found in the Report.pdf.  
This project is a fully automated data collection package for different control parameter  
sampling methods. These control parameters are sampled for a global best particle swarm  
optimization algorithm and obey the equality proposed by the Poli stability condition  
(this is cited in the report). They are denoted as c1, c2 and w.  
The algorithm is run on four different benchmark tests. These are Ripple 25, Exponential, Discus and Rosenbrock.  
The methods by which these are sampled are graphed and compared in a jupyter notebook, PSOData.ipynb.

The algorithm is initialised with thirty particles randomly placed in a ten dimensional search space.  
Each approach is run for five-hundred iterations and twenty independent runs. The personal best  
of each particle is sampled at every tenth iteration and recorded in a csv file. The average of all the particle's   
personal bests at each tenth iteration is then graphed in a jupyter notebook. 

### Summary of Sampling Approaches

- Fixed Sampling: The parameters aren't resampled and are statically set to what is  
regarded as values of best performance (c1 = 1.4, c2 = 1.4and w = 0.7).
-  Iterative Sampling: The parameters are resampled at every iteration for every particle.  
Their values are generated from a weighted  randomisation.
- Stagnation Sampling: The parameters are resampled when the personal best of a particle  
has stagnated for three consecutive iterations.
- Stability Region Sampling: The iterative and stagnation based variants of this  
sample from a stability region defined by an inequality. This inequality proposes  
an optimal region for control parameter sampling in hopes to eliminate the need for tuning.

### The Repository

The repository consists of the readme and the src directory. The src directory consists of python  
files representing benchmark function and particle objects as well as a helper function file.  
The setPSO file acts as a class file that is an object representing a type of sampling approach at  
an instance. It also contains the jupyter-notebook file used to graph all relevant data and the  
run.sh file for running the fixed parameter PSO.  
A report on the project theory and outcome is located in the root directory.

### To Run

In a terminal window

``$ cd src``

``$ sh run.sh``

### run.sh

The shell script generates directories of csv data for each approach. The entire process is automated, therefore  
just wait until the program completes running to view the data in the jupyter-notebook.